/*
 * ATLauncher - https://gitlab.com/acidpointer/atlauncher
 * Copyright (C) 2022 ATLauncher FORK
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package com.atlauncher.data;

import java.util.UUID;

public class OfflineAccount extends AbstractAccount{
    private final String accessToken;
    private final String sessionToken;

    public OfflineAccount(String username) {
        this.username = username;
        this.minecraftUsername = username;
        this.accessToken = UUID.randomUUID().toString();
        this.sessionToken = UUID.randomUUID().toString();
        this.uuid = UUID.randomUUID().toString();
    }

    public void setUsername(String username) {
        this.minecraftUsername = username;
        this.username = username;
    }

    @Override
    public boolean allowSkinUpdating() {
        return false;
    }

    @Override
    public String getAccessToken() {
        return this.accessToken;
    }

    @Override
    public String getSessionToken() {
        return this.sessionToken;
    }

    @Override
    public String getUserType() {
        return "mojang";
    }

    @Override
    public String getCurrentUsername() {
        return this.username;
    }

    @Override
    public void updateSkinPreCheck() {

    }

    @Override
    public void changeSkinPreCheck() {

    }

    @Override
    public String getSkinUrl() {
        return "null";
    }
}
