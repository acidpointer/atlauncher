/*
 * ATLauncher - https://gitlab.com/acidpointer/atlauncher
 * Copyright (C) 2022 ATLauncher FORK
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.atlauncher.gui.components;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.UIManager;

import com.atlauncher.evnt.listener.ThemeListener;
import com.atlauncher.evnt.manager.ThemeManager;
import com.atlauncher.managers.LogManager;
import com.atlauncher.utils.OS;

public abstract class BottomBar extends JPanel implements ThemeListener {
    private static final long serialVersionUID = -7488195680365431776L;

    protected final JButton gitlabIcon = new SMButton("/assets/image/social/gitlab.png",
        "GitLab");

    protected final JButton githubIcon = new SMButton("/assets/image/social/github.png", "GitHub (Original source)");

    protected final JPanel rightSide = new JPanel(new FlowLayout(FlowLayout.RIGHT, 5, 8));

    public BottomBar() {
        super(new BorderLayout());
        this.setBorder(BorderFactory.createMatteBorder(1, 0, 0, 0, UIManager.getColor("BottomBar.dividerColor")));
        this.setPreferredSize(new Dimension(0, 50));

        this.add(this.rightSide, BorderLayout.EAST);
        this.setupSocialButtonListeners();
        this.rightSide.add(this.gitlabIcon);
        this.rightSide.add(this.githubIcon);
        ThemeManager.addListener(this);
    }

    private void setupSocialButtonListeners() {
        githubIcon.addActionListener(e -> {
            LogManager.info("Opening Up ATLauncher GitHub Page");
            OS.openWebBrowser("https://atl.pw/github-launcher-3");
        });
        githubIcon.addActionListener(e -> {
            LogManager.info("Opening Up ATLauncherFork GitLab");
            OS.openWebBrowser("https://gitlab.com/acidpointer/atlauncher");
        });
    }

    public void onThemeChange() {
        this.setBorder(BorderFactory.createMatteBorder(1, 0, 0, 0, UIManager.getColor("BottomBar.dividerColor")));
    }
}
